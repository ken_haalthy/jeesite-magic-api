/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.jeesite.modules.magic.api.db;

import com.jeesite.common.tests.BaseInitDataTests;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * 初始化MagicApi菜单
 * @author ThinkGem
 */
@Component
@ConditionalOnProperty(name="jeesite.initdata", havingValue="true", matchIfMissing=false)
public class InitMagicApiData extends BaseInitDataTests {

	@Override
	public boolean initData() throws Exception {
		this.initModuleInfo("magic-api");
		this.initModuleMenu("//magic/web");
		return true;
	}

}
