
## 介绍

[magic-api](https://gitee.com/ssssssss-team/magic-api) 是一个基于Java的接口快速开发框架，编写接口将通过magic-api提供的UI界面完成，自动映射为HTTP接口，无需定义Controller、Service、Dao、Mapper、XML、VO等Java对象即可完成常见的HTTP API接口开发。

完美搭配 [可视化数据大屏](https://jeesite.com/docs/visual/) 提供数据接口。 

**特性**

- 支持MySQL、MariaDB、Oracle、DB2、PostgreSQL、SQLServer 等支持jdbc规范的数据库
- 支持非关系型数据库Redis、Mongodb
- 支持集群部署、接口自动同步。
- 支持分页查询以及自定义分页查询
- 支持多数据源配置，支持在线配置数据源
- 支持SQL缓存，以及自定义SQL缓存
- 支持自定义JSON结果、自定义分页结果
- 支持对接口权限配置、拦截器等功能
- 支持运行时动态修改数据源
- 支持Swagger接口文档生成
- 基于[magic-script](https://gitee.com/ssssssss-team/magic-script)脚本引擎，动态编译，无需重启，实时发布
- 支持Linq式查询，关联、转换更简单
- 支持数据库事务、SQL支持拼接，占位符，判断等语法
- 支持文件上传、下载、输出图片
- 支持脚本历史版本对比与恢复
- 支持脚本代码自动提示、参数提示、悬浮提示、错误提示
- 支持导入Spring中的Bean、Java中的类
- 支持在线调试
- 支持自定义工具类、自定义模块包、自定义类型扩展、自定义方言、自定义列名转换等自定义操作


## 快速运行

1. 环境准备：`JDK 1.8 or 11、17`、`Maven 3.6+`、无需准备数据库（使用内嵌 H2 DB）
2. 下载源码：<https://gitee.com/thinkgem/jeesite-magic-api/repository/archive/main.zip>
3. 执行脚本：`/modules/magic-api/bin/package.bat(sh)` 编译打包 Magic API 模块
4. 执行脚本：`/web-fast/bin/run-tomcat.bat(sh)` 启动 Web 服务（会自动初始化库）
5. 浏览器访问：<http://127.0.0.1:8980/>  账号 system 密码 admin
6. 部署常见问题：<https://jeesite.com/docs/faq/>

## 快速开始

三分钟写出查询接口

1. 创建分组

点击创建分组按钮后，输入分组信息，点击创建。

2. 新建接口

右键分组，点击新建接口。

在编辑器输入内容后，填写接口名称和及其路径。

```js
var sql = """
select * from js_sys_user
"""
return db.select(sql)
```

右上角点击保存按钮保存接口。

3. 访问接口

右上角点击运行按钮，查看执行结果。

## MagicAPI 文档

* 文档地址：[https://ssssssss.org](https://ssssssss.org)
* 在线演示：[https://magic-api.ssssssss.org](https://magic-api.ssssssss.org)

## JeeSite 文档

* 部署文档：http://jeesite.com/docs/install-deploy/
* 常见问题：http://jeesite.com/docs/faq/
* 更多文档：http://jeesite.com/docs

## JeeSite 介绍

JeeSite 快速开发平台，不仅仅是一个后台开发框架，它是一个企业级快速开发解决方案，后端基于经典组合 Spring Boot、Shiro、MyBatis，前端采用 Beetl、Bootstrap、AdminLTE 经典开发模式，或者分离版 Vue3、Vite、Ant Design Vue、TypeScript、Vben Admin 最先进技术栈。

提供在线代码生成功能，可自动创建业务模块工程和微服务模块工程，自动生成前端代码和后端代码；包括功能模块如：组织机构、角色用户、菜单及按钮授权、数据权限、系统参数、内容管理、工作流等。

采用松耦合设计，微内核和插件架构，模块增减便捷；界面细节到位，一键换肤；众多账号安全设置，密码策略；文件在线预览；消息推送；多元化第三方登录；在线定时任务配置；支持集群，支持SAAS；支持多数据源；支持读写分离、分库分表；支持微服务应用。

主要目的是能够让初级的研发人员快速的开发出复杂的业务功能，中高级人员有时间做一些更有用的事情。让开发者注重专注业务，其余有平台来封装技术细节，降低技术难度，从而节省人力成本，缩短项目周期，提高软件安全质量。

JeeSite 自 2013 年发布以来已被广大爱好者用到了企业、政府、医疗、金融、互联网等各个领域中，拥有：精良架构、易于扩展、大众思维的设计模式，工匠精神，用心打磨每一个细节，深入开发者的内心，并荣获开源中国《最受欢迎中国开源软件》多次奖项，期间也帮助了不少刚毕业的大学生，教师作为入门教材，快速的去实践。

2019 年换代升级，作者结合了多年总结和经验，以及各方面的应用案例，对架构完成了一次全部重构，也纳入很多新的思想。不管是从开发者模式、底层架构、逻辑处理还是到用户界面，用户交互体验上都有很大的进步，在不忘学习成本、提高开发效率的情况下，安全方面也做和很多工作，包括：身份认证、密码策略、安全审计、日志收集等众多安全选项供您选择。努力为大中小微企业打造全方位企业级快速开发解决方案。

2021 年终发布 Vue3 的前后分离版本，使得 JeeSite 拥有同一个后台服务 Web 来支撑分离版和全栈版两套前端技术栈。

**优势**

JeeSite 非常易于二次开发，可控性高，整体架构清晰、技术稳定而先进、源代码书写规范、经典技术会的人多、易于维护、易于扩展、安全稳定。

JeeSite 功能全，知识点非常多，也非常少。因为她使用的都是一些通用的技术，通俗的设计风格，大多数基础知识点，多数人都能掌握，所以每一个 JeeSite 的功能点都非常容易掌握。只要您学会使用这些功能和组件的应用，就可以顺利的完成系统开发了。

JeeSite 是一个低代码开发平台，具有较高的封装度、扩展性，封装不是限制您去做一些事情，而是在便捷的同时，也具有较好的扩展性，在不具备一些功能的情况下，JeeSite 提供了扩展接口，提供了原生调用方法。

大家都在用 Spring，也在学习 Spring 的优点，Spring 提供了较好的扩展性，可又有多少人去修改它的源代码呢，退一步说，大家去修改了 Spring 的源码，反而会对未来升级造成很大困扰，您说不是呢？这样的例子很多，所以不要纠结，我们非常注重这一点，JeeSite 也一样具备强大的扩展性。

为什么说 JeeSite 比较易于学习？JeeSite 很好的把握了设计的 “度”，避免过度设计的情况。过度设计是在产品设计过程中忽略了产品和用户的实际需求，反而带来了不必要的复杂性，而忽略了系统的学习、开发和维护成本。

* 至今 JeeSite 平台架构已经非常稳定。
* JeeSite 精益求精，用心打磨每一个细节。
* JeeSite 是一个专业的平台，是一个让你使用放心的平台。
* 社区版基于 Apache License 2.0 开源协议，永久免费使用。

### 架构特点及安全方面的优势：<https://jeesite.com/docs/feature/>

## JeeSite 生态系统

* 分布式微服务（Spring Cloud）：<https://gitee.com/thinkgem/jeesite4-cloud>
* Flowable业务流程引擎（BPM）：<http://jeesite.com/docs/bpm/>
* JFlow工作流引擎：<https://gitee.com/thinkgem/jeesite4-jflow>
* 多站点内容管理模块（CMS）：<https://jeesite.com/docs/cms/>
* 手机端移动端：<https://gitee.com/thinkgem/jeesite4-uniapp>
* PC客户端程序：<https://gitee.com/thinkgem/jeesite-client>
* Vue3分离版本：<https://gitee.com/thinkgem/jeesite-vue>
* JeeSite统一认证：<https://jeesite.com/docs/oauth2-server>
* TopIAM统一认证：<https://gitee.com/thinkgem/jeesite-topiam>
* MaxKey统一认证：<https://gitee.com/thinkgem/jeesite-maxkey>
* MybatisPlus: <https://gitee.com/thinkgem/jeesite-mybatisplus>
* Magic接口快速开发：<https://gitee.com/thinkgem/jeesite-magic-api>
* 内外网中间件：<https://my.oschina.net/thinkgem/blog/4624519>
